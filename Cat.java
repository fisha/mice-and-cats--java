package pl.edu.pg;

import java.util.Random;

public class Cat extends Animal {

    private static int catCount = 1;
    private int catMove;
    private int catId;


    public Cat(int posX, int posY) {
        super(posX, posY);
        this.catId = catCount++;
        this.name = " K" + catId;
    }

    @Override
    public void info() {
        System.out.println("Kot " + name + ", pozycja(" + posX + "," + posY + ")");
    }

    @Override
    public void position() {
        System.out.print("(" + posX + "," + posY + ")");
    }

    @Override
    public void beginning(String[][] table, int areaSize, int numberOfAnimals) {
        this.areaSize = areaSize;
        Random rand = new Random();
        posX = rand.nextInt(areaSize);
        posY = rand.nextInt(areaSize);
        table[posX][posY] = name;
    }

    @Override
    public void move(String[][] table, int areaSize) {
        Random rand = new Random();
        catMove = rand.nextInt(5) + 1; //losuje od 1 do 4

        switch (catMove) {
            case 1:
                posX = Math.abs(posX - 1);
                break;
            case 2:
                posY = posY + 1;
                if (posY == areaSize) {
                    posY = areaSize - 2;
                }
                break;
            case 3:
                posX = posX + 1;
                if (posX == areaSize) {
                    posX = areaSize - 2;
                }
                break;
            case 4:
                posY = Math.abs(posY - 1);
                break;
            default:
                posX = posX;
                posY = posY;
        }


        table[posX][posY] = name;


    }


    //////// settery i gettery /////////////////////////
    public void setCatId(int mouseId) {
        this.catId = mouseId;
    }

    public int getCatId() {
        return catId;
    }


}
