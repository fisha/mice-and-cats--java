package pl.edu.pg;

public interface Printable {
    void print(String[][] table);
}
