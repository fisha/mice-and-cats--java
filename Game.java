package pl.edu.pg;

import java.util.ArrayList;
import java.util.List;


public class Game {


    public void start() {

        System.out.println("Gra uruchomiona!\n");

        int boardSize = 7;
        int maxMice = 6;
        int maxCats = 6;
        int drawPeriod = 1;
        int drawCount = 0;
        int round = 1;
        String outcome = "start";
        int miceAlive = maxMice;
        int catsAlive = maxCats;


//stworzenie planszy

        Board board = new Board(boardSize);
        String[][] table = new String[boardSize][boardSize];
        System.out.println("Plansza bez zwierząt:");
        board.clear(table);
        board.print(table);

//utworzenie obiektów kotów i myszy
        List<Cat> cats = new ArrayList<>();
        for (int i = 0; i < maxCats; i++) {
            cats.add(new Cat(0, 0));
        }

        List<Mouse> mice = new ArrayList<Mouse>();
        for (int i = 0; i < maxMice; i++) {
            mice.add(new Mouse(0, 0));
        }



/*
//sprawdzenie, czy utworzono obiekty (później można wykomentować)
        System.out.println("\nWszystkie koty:");
        for (Cat cat : cats) {
            cat.info();
        }
        System.out.println();

        System.out.println("Wszystkie myszy:");
        for (Mouse mouse : mice) {
            mouse.info();
        }
        System.out.println();
*/

//rozstawienie zwierząt na planszy

        System.out.println("\nPozycje początkowe kotów:");
        for (Cat cat : cats) {
            cat.beginning(table, boardSize, maxCats);
            cat.info();
        }
        System.out.println();


        System.out.println("Pozycje początkowe myszy:");
        for (int i = 0; i < (maxMice / 4); i++) {

            if (mice.get(i).getMouseId() > 0) {
                mice.get(i).beginning(table, boardSize, maxMice);
                mice.get(i).posX = 0;
            }
        }

        for (int i = maxMice / 4; i < maxMice / 2; i++) {
            if (mice.get(i).getMouseId() > 0) {
                mice.get(i).beginning(table, boardSize, maxMice);
                mice.get(i).posY = 0;
            }
        }

        for (int i = maxMice / 2; i < maxMice * 3 / 4; i++) {
            if (mice.get(i).getMouseId() > 0) {
                mice.get(i).beginning(table, boardSize, maxMice);
                mice.get(i).posX = boardSize - 1;
            }
        }

        for (int i = maxMice * 3 / 4; i < maxMice; i++) {
            if (mice.get(i).getMouseId() > 0) {
                mice.get(i).beginning(table, boardSize, maxMice);
                mice.get(i).posY = boardSize - 1;
            }
        }


        for (int i = 0; i < maxMice; i++) {
            if (mice.get(i).getMouseId() > 0) {
                mice.get(i).info();
            }
        }


        for (int i = 0; i < maxMice; i++) {
            if (mice.get(i).getMouseId() > 0) {
                table[mice.get(i).posX][mice.get(i).posY] = mice.get(i).name;
            }
        }

        board.print(table);
        System.out.println("--------------------------");


//ROZPOCZĘCIE RUCHÓW //////////////////////////////////////////////////////////

        while (!outcome.equals("KONIEC GRY!")) {

            board.clear(table);
            System.out.println("Runda " + round + ":");


            for (int i = 0; i < maxMice; i++) {
                if (mice.get(i).getMouseId() > 0) {
                    System.out.print("Mysz " + mice.get(i).name + " ");
                    mice.get(i).position();
                    System.out.print(" -> ");

                    mice.get(i).move(table, boardSize);
                    mice.get(i).position();
                    System.out.println();
                    table[mice.get(i).posX][mice.get(i).posY] = mice.get(i).name;
                }
            }


            for (int i = 0; i < maxCats; i++) {

                if (cats.get(i).getCatId() > 0) {
                    System.out.print("Kot " + cats.get(i).name + " ");
                    cats.get(i).position();
                    System.out.print(" -> ");

                    cats.get(i).move(table, boardSize);
                    cats.get(i).position();
                    System.out.println();
                }

            }


            System.out.println();


            //////KOT ZJADA MYSZ ////////////////////////////////////

            /*
            for (int i = 0; i < maxCats; i++) {
                for (int j = 0; j < maxMice; j++) {

                    if (cats.get(i).posX == mice.get(j).posX
                            & cats.get(i).posY == mice.get(j).posY) {


                        if (miceAlive == 1
                                & cats.get(i).posX == boardSize / 2
                                & cats.get(i).posY == boardSize / 2
                                & mice.get(j).posX == boardSize / 2
                                & mice.get(j).posY == boardSize / 2) {

                            outcome = "KONIEC GRY!";
                            System.out.println("GRA ZAKOŃCZONA!\nREMIS!");
                            board.print(table);
                            return;

                        } else {
                            miceAlive--;
                            mice.get(j).setMouseId(-1);
                            mice.get(j).posX = 0;
                            mice.get(j).posY = 0;
                            System.out.println("Kot " + cats.get(i).name + " złapał mysz " + mice.get(j).name);
                        }

                    }
                }
            }

            if (miceAlive < 0) {
                miceAlive = 0;
            }

            System.out.println("Liczba pozostałych myszy: " + miceAlive);


            mice.get(2).posX = 2;
            mice.get(2).posY = 2;
            mice.get(3).posX = 2;
            mice.get(3).posY = 2;
            cats.get(2).posX = 2;
            cats.get(2).posY = 2;
*/

            for (int i = 0; i < maxCats; i++) {
                for (int j = 0; j < maxMice; j++) {
                    for (int k = 0; k < maxMice; k++) {

                        if (cats.get(i).posX == mice.get(j).posX
                                & cats.get(i).posY == mice.get(j).posY
                                & mice.get(k).posX == mice.get(j).posX
                                & mice.get(k).posY == mice.get(j).posY
                                & cats.get(i).getCatId() > 0
                                & mice.get(j).getMouseId() > 0
                                & mice.get(k).getMouseId() > 0
                                & j!= k) {

                            catsAlive--;
                            cats.get(i).setCatId(-1);
                            cats.get(i).posX = 0;
                            cats.get(i).posY = 0;
                            System.out.println("Myszy " + mice.get(j).name + " i " + mice.get(k).name + " złapały kota " + cats.get(i).name);

                        }
                    }
                }
            }


            for (int i = 0; i < maxCats; i++) {
                for (int j = 0; j < maxMice; j++) {


                    if (cats.get(i).posX == mice.get(j).posX
                            & cats.get(i).posY == mice.get(j).posY
                            & mice.get(j).getMouseId() > 0) {


                        if (miceAlive == 1
                                & cats.get(i).posX == boardSize / 2
                                & cats.get(i).posY == boardSize / 2
                                & mice.get(j).posX == boardSize / 2
                                & mice.get(j).posY == boardSize / 2) {

                            outcome = "KONIEC GRY!";
                            System.out.println("GRA ZAKOŃCZONA!\nREMIS!");
                            board.print(table);
                            return;

                        } else {
                            miceAlive--;
                            mice.get(j).setMouseId(-1);
                            mice.get(j).posX = 0;
                            mice.get(j).posY = 0;
                            System.out.println("Kot " + cats.get(i).name + " złapał mysz " + mice.get(j).name);
                        }

                    }
                }
            }


            if (miceAlive < 0) {
                miceAlive = 0;
            }

            System.out.println("Liczba pozostałych myszy: " + miceAlive);

            if (catsAlive < 0) {
                catsAlive = 0;
            }

            System.out.println("Liczba pozostałych kotów: " + catsAlive);


            for (int i = 0; i < maxCats; i++) {
                for (int j = 0; j < maxMice; j++) {

                    if (cats.get(i).posX != boardSize / 2
                            & cats.get(i).posY != boardSize / 2
                            & mice.get(j).posX == boardSize / 2
                            & mice.get(j).posY == boardSize / 2) {

                        outcome = "KONIEC GRY!";
                        System.out.println("Mysz" + mice.get(j).name + " dotarła do sera.\n" + "GRA ZAKOŃCZONA!\nWYGRAŁY MYSZY!");
                        board.print(table);
                        return;

                    }
                }

            }


            if (miceAlive < 1) {
                outcome = "KONIEC GRY!";
                System.out.println("GRA ZAKOŃCZONA!\nWYGRAŁY KOTY!");
                board.print(table);

            }

            drawCount++;
            if (drawCount == drawPeriod) {
                board.print(table);
                drawCount = 0;
            }

            round++;

        }
    }


}
