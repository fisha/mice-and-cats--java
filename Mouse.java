package pl.edu.pg;

import java.util.Random;

public class Mouse extends Animal {

    private static int mouseCount = 1;
    private int mouseMove;
    private int mouseId;


    public Mouse(int posX, int posY) {
        super(posX, posY);
        this.mouseId = mouseCount++;
        this.name = " M" + mouseId;

    }

    @Override
    public void info() {
        System.out.println("Mysz " + name + ", pozycja(" + posX + "," + posY + ")");
    }

    @Override
    public void position() {
        System.out.print("(" + posX + "," + posY + ")");
    }

    @Override
    public void beginning(String[][] table, int areaSize, int numberOfAnimals) {
        this.areaSize = areaSize;
        Random rand = new Random();
        posX = rand.nextInt(areaSize);
        posY = rand.nextInt(areaSize);
    }

    @Override
    public void move(String[][] table,int areaSize) {
        Random rand = new Random();
        mouseMove = rand.nextInt(9) + 1; //losuje od 1 do 8

        switch (mouseMove) {
            case 1:
                posX = Math.abs(posX - 1);
                posY = posY + 1;
                if (posY == areaSize) {
                    posY = areaSize - 2;
                }
                break;
            case 2:
                posY = posY + 1;
                if (posY == areaSize) {
                    posY = areaSize - 2;
                }
                break;
            case 3:
                posX = posX + 1;
                if (posX == areaSize) {
                    posX = areaSize - 2;
                }
                posY = posY + 1;
                if (posY == areaSize) {
                    posY = areaSize - 2;
                }
                break;
            case 4:
                posX = posX + 1;
                if (posX == areaSize) {
                    posX = areaSize - 2;
                }
                break;
            case 5:
                posX = posX + 1;
                if (posX == areaSize) {
                    posX = areaSize - 2;
                }
                posY = Math.abs(posY - 1);
                break;
            case 6:
                posY = Math.abs(posY - 1);
                break;
            case 7:
                posX = Math.abs(posX - 1);
                posY = Math.abs(posY - 1);
                break;
            case 8:
                posX = Math.abs(posX - 1);
                break;
            default:
                posX = posX;
                posY = posY;
        }


    }

//////// settery i gettery /////////////////////////
    public void setMouseId(int mouseId) {
        this.mouseId = mouseId;
    }

    public int getMouseId() {
        return mouseId;
    }

}
