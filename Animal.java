package pl.edu.pg;

public abstract class Animal {

    protected int posX;
    protected int posY;
    protected String name;
    protected int areaSize;

    public Animal(int posX, int posY) {
        this.posX = posX;
        this.posY = posY;
    }

    public abstract void info();
    public abstract void position();
    public abstract void beginning(String[][] table, int areaSize, int numberOfAnimals);
    public abstract void move(String[][] table, int areaSize);


}
