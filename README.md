
Mamy do dyspozycji kwadratową planszę o nieparzystym rozmiarze (2 < BOARD_SIZE < 100).
Na środku planszy znajduje się ser.
Równomiernie, przy krawędziach planszy rozmieszczone jest MAX_MICE (1 < MAX_MICE < 100) myszy, oraz MAX_CATS (1 < MAX_CATS < 100) kotów - losowo w wolnych miejscach.
Symulacja podzielona jest na tury. W każdej turze każda mysz i każdy kot przemieszczają się na losowe, sąsiednie pole, przy czym myszy maja do wyboru 8 pól (góra, dół, na boki i na ukos), a koty tylko 4 pola (góra, dół i na boki).
Koty i myszy nie mogą wychodzić poza planszę, ale mogą chodzić po polu sera, a na jednym polu może być kilka postaci na raz.
Jeśli po danej turze na danym polu znajduje się na raz kot(y) i mysz(y) to mysz(y) zostają złapane i znikają z planszy.
Grę wygrywają myszy, jeśli którakolwiek z nich dotrze do sera.
Grę wygrywają koty, jeśli wszystkie myszy zostaną złapane nim dotrą do sera.
Wyjątek: w przypadku, gdy w danej turze na polu sera pojawi się na raz kot i mysz (i jest to ostatnia z myszy na planszy) - ogłasza się remis.

Liczby BOARD_SIZE, MAX_MICE oraz MAX_CATS zdefiniowano jako stałe.
Myszy i koty są ponumerowane.

Dodatkowo w konsoli pojawiają się komunikaty specjalne, takie jak:
"Runda 10"
"Liczba pozostałych myszy: 2"
"Liczba pozostałych kotów: 6"
"Mysz M5 dotarła do sera."
itp.


Po wygenerowaniu planszy i pozycji startowych wszystkich postaci, plansza została wyrysowana na konsoli (tekstowo) oraz aktualizuje się po każdym ruchu.