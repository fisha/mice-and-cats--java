package pl.edu.pg;

public class Board implements Printable{

    protected int size;
    protected int i;
    protected int j;

    public Board(int size){
        this.size = size;
    }

    public void clear(String[][] table) {
        System.out.println();
        for (i = 0; i < size; i++) {
            for (j = 0; j < size; j++) {
                table[i][j] = " . ";
                table[size/2][size/2] = "(S)";
            }
        }
    }



    @Override
    public void print(String[][] table) {

        System.out.println();
        for (i = 0; i < size; i++) {
            for (j = 0; j < size; j++) {

                System.out.print(table[i][j]);
            }
            System.out.println();

        }
        System.out.println();

    }

    /*
    @Override
    public void print() {

        String[][] table = new String[size][size];
        System.out.println();
        for (i = 0; i < size; i++) {
            for (j = 0; j < size; j++) {
                table[i][j] = " . ";
                table[size/2][size/2] = "(S)";
                System.out.print(table[i][j]);
            }
            System.out.println();

        }


    }
    */




}
